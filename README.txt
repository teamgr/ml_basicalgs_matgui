Authors:
Giulia Pasquale (giulia [dot] pasquale [at] iit [dot] it)
Raffaello Camoriano (raffaello [dot] camoriano [at] iit [dot] it)

TO DO in the future (higher priority):

- at each change in the dataset panel, reset train and test panels
- make average prediction confidence visible/enabled ONLY for LR and KLR
- plot on the figures clearer info about the algorithm/dataset to associate figure <--> dataset+algo+hyperparameters 
- uniform the aspect of the test and dataset figures

TO DO in the future (lower priority):

- make the intersection between training and validation error surfaces clearer 
- final comparison table among different algorithms run on the SAME dataset
- load weights (w/c) and/or hyperparameters (lambda/sigma)
- callback to fix optimal kernel/regularization hyperparameter and plot wrt the other parameter in 2D