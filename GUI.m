function varargout = test(varargin)
% TEST MATLAB code for test.fig
%      TEST, by itself, creates a new TEST or raises the existing
%      singleton*.
%
%      H = TEST returns the handle to a new TEST or the handle to
%      the existing singleton*.
%
%      TEST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TEST.M with the given input arguments.
%
%      TEST('Property','Value',...) creates a new TEST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before test_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to test_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help test

% Last Modified by GUIDE v2.5 21-Apr-2014 19:43:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @test_OpeningFcn, ...
                   'gui_OutputFcn',  @test_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function test_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to test (see VARARGIN)

% Choose default command line output for test
handles.output = hObject;

set(handles.uipanel_sourceDataset,'SelectedObject',handles.radiobutton_generatedDataset);

set(handles.pushbutton_generateDataset, 'Visible', 'Off');
set(handles.pushbutton_loadDataset, 'Visible', 'Off');

set(handles.pushbutton_saveBothDataset, 'Visible', 'Off');
set(handles.pushbutton_saveDataDataset, 'Visible', 'Off');
set(handles.pushbutton_saveFigDataset, 'Visible', 'Off');
set(handles.pushbutton_saveBothDataset, 'Position', [78.83 1.312 15 1.625]);
set(handles.pushbutton_saveFigDataset, 'Position', [63.167 0.5 15 1.625]);
set(handles.pushbutton_saveDataDataset, 'Position', [63.167 2.312 15 1.625]);

set(handles.pushbutton_plotDataset, 'Visible', 'Off');
set(handles.pushbutton_plotDataset, 'Position', [40.5 1 15 2.5]);

set(handles.edit_nTe, 'Visible', 'Off');
set(handles.edit_nTe, 'String', '');
set(handles.edit_D, 'Visible', 'Off');
set(handles.edit_D, 'String', '');
set(handles.edit_nTr, 'Visible', 'Off');
set(handles.edit_nTr, 'String', '');
set(handles.edit_flipPercent, 'Visible', 'Off');
set(handles.edit_flipPercent, 'String', '');

set(handles.text_nTe, 'Visible', 'Off');
set(handles.text_nTr, 'Visible', 'Off');
set(handles.text_D, 'Visible', 'Off');
set(handles.text_flipPercent, 'Visible', 'Off');

set(handles.text_kindDataset, 'Visible', 'On');
set(handles.popupmenu_kindDataset, 'Visible', 'On');
set(handles.popupmenu_kindDataset, 'Value', 1);
set(handles.text_NGaussians, 'Visible', 'Off');
set(handles.edit_NGaussians, 'Visible', 'Off');
set(handles.uitable_datasetSpecs, 'Visible', 'Off');

handles.figDataset = [];
handles.figParams = [];
handles.figResults = [];

guidata(hObject, handles);

addpath(genpath('.'));

function varargout = test_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function uipanel_paramsel_type_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel_paramsel_type 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

switch get(hObject,'String')
    
    case 'Manual'
        set( findall(handles.uipanel_manual_paramsel, '-property', 'Enable'), 'Enable', 'on')
        set( findall(handles.uipanel_holdOut_paramsel, '-property', 'Enable'), 'Enable', 'off')

    case 'Hold out'
        set( findall(handles.uipanel_manual_paramsel, '-property', 'Enable'), 'Enable', 'off')            
        set( findall(handles.uipanel_holdOut_paramsel, '-property', 'Enable'), 'Enable', 'on')  
end

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---'); 

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');

function uipanel_manual_paramsel_CreateFcn(hObject, eventdata, handles)

set( findall(hObject, '-property', 'Enable'), 'Enable', 'on')
function uipanel_holdOut_paramsel_CreateFcn(hObject, eventdata, handles)

set( findall(hObject, '-property', 'Enable'), 'Enable', 'off')

function popupmenu_algorithm_Callback(hObject, eventdata, handles)

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');


set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');     

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

contents = cellstr(get(hObject,'String'));
algo = contents{get(hObject,'Value')};
 
if  strcmp(algo,'Kernel Regularized Least Squares') || ...
    strcmp(algo,'Kernel Logistic Regression')

    % Activate kernel selection popupmenu
    set(handles.popupmenu_kernel,'Enable','on')

    % Get selected kernel
    kerlist = get(handles.popupmenu_kernel, 'String');
    ker = get(handles.popupmenu_kernel, 'Value');
    
    if strcmp(kerlist{ker},'Linear Kernel') == 0
        % Activate kernel parameter geometrical sequence checkbox
        set(handles.checkbox_geomKer,'Visible','on')

        % Activate kernel parameter selection elements
        set(handles.text_kerPar,'Visible','on')
        set(handles.edit_kerPar,'Visible','on')
    
        % Activate HO kernel parameter options and output text
        set(handles.text_rangeKer,'Visible','on')
        set(handles.edit_minKer,'Visible','on')
        set(handles.edit_maxKer,'Visible','on')
        set(handles.edit_kerStep,'Visible','on')
        set(handles.text_bestKerPar,'Visible','on')
    end
else
    % Deactivate kernel selection popupmenu
    set(handles.popupmenu_kernel,'Enable','off')

    % Deactivate kernel parameter geometrical sequence checkbox
    set(handles.checkbox_geomKer,'Visible','off')

    % Deactivate kernel parameter selection elements
    set(handles.text_kerPar,'Visible','off')
    set(handles.edit_kerPar,'Visible','off')
    
    % Deactivate fixed parameter view of the validation/test error
    set(handles.checkbox_fixKerParam,'Visible','off')
    set(handles.checkbox_fixRegParam,'Visible','off')
    
    % Deactivate HO kernel parameter options and output text
    set(handles.text_rangeKer,'Visible','off')
    set(handles.edit_minKer,'Visible','off')
    set(handles.edit_maxKer,'Visible','off')
    set(handles.edit_kerStep,'Visible','off')
    set(handles.text_bestKerPar,'Visible','off')

end

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');

guidata(hObject,handles);
function popupmenu_algorithm_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function popupmenu_kernel_Callback(hObject, eventdata, handles)

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');           

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');

% Get selected kernel
kerlist = get(hObject, 'String');
ker = get(hObject, 'Value');

if strcmp(kerlist{ker},'Linear Kernel') == 0
    % Activate kernel parameter geometrical sequence checkbox
    set(handles.checkbox_geomKer,'Visible','on')

    % Activate kernel parameter selection elements
    set(handles.text_kerPar,'Visible','on')
    set(handles.edit_kerPar,'Visible','on')

    % Activate HO kernel parameter options and output text
    set(handles.text_rangeKer,'Visible','on')
    set(handles.edit_minKer,'Visible','on')
    set(handles.edit_maxKer,'Visible','on')
    set(handles.edit_kerStep,'Visible','on')
    set(handles.text_bestKerPar,'Visible','on')
else
    % Activate kernel parameter geometrical sequence checkbox
    set(handles.checkbox_geomKer,'Visible','off')

    % Activate kernel parameter selection elements
    set(handles.text_kerPar,'Visible','off')
    set(handles.edit_kerPar,'Visible','off')

    % Activate HO kernel parameter options and output text
    set(handles.text_rangeKer,'Visible','off')
    set(handles.edit_minKer,'Visible','off')
    set(handles.edit_maxKer,'Visible','off')
    set(handles.edit_kerStep,'Visible','off')
    set(handles.text_bestKerPar,'Visible','off')
end

guidata(hObject,handles);
function popupmenu_kernel_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'Enable','off')

function edit_valPerc_Callback(hObject, eventdata, handles)

valPerc = str2double(get(handles.edit_valPerc,'String'));
if isnan(valPerc) || valPerc<0 || valPerc>100
    errordlg('You must enter a value in [0,100] for Validation Split %','Invalid Input','modal')
    set(handles.edit_valPerc,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');      

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');

function edit_valPerc_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function edit_HORepetitions_Callback(hObject, eventdata, handles)
HORepetitions = str2double(get(handles.edit_HORepetitions,'String'));
if isnan(HORepetitions) || HORepetitions<0 || HORepetitions==0 || HORepetitions-floor(HORepetitions)>0
    errordlg('You must enter a positive integer value for Repetitions','Invalid Input','modal')
    set(handles.edit_HORepetitions,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');   

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');
function edit_HORepetitions_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_regPar_Callback(hObject, eventdata, handles)
regPar = str2double(get(handles.edit_regPar,'String'));
if isnan(regPar) || regPar<0
    errordlg('You must enter a positive value (or 0) for Reg. Parameter','Invalid Input','modal')
    set(handles.edit_regPar,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');    

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');
function edit_regPar_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function edit_kerPar_Callback(hObject, eventdata, handles)
kerPar = str2double(get(handles.edit_kerPar,'String'));

% Get selected kernel
kerlist = get(handles.popupmenu_kernel, 'String');
ker = get(handles.popupmenu_kernel, 'Value');

if (isnan(kerPar) || kerPar<0 ) && strcmp(kerlist{ker},'Linear Kernel') == 0
    errordlg('You must enter a positive value (or 0) for Ker. Parameter','Invalid Input','modal')
    set(handles.edit_kerPar,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');  

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');
function edit_kerPar_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'Visible','off');
function text_kerPar_CreateFcn(hObject, eventdata, handles)

set(hObject,'Visible','off');

function edit_minReg_Callback(hObject, eventdata, handles)
minReg = str2double(get(handles.edit_minReg,'String'));
maxReg = str2double(get(handles.edit_maxReg,'String'));
regStep = str2double(get(handles.edit_regStep,'String'));
if isnan(minReg) || minReg<0
    errordlg('You must enter a positive value (or 0) for minimum Reg. Parameter','Invalid Input','modal')
    set(handles.edit_minReg,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
if maxReg<minReg
    errordlg('Value for maximum Reg. Parameter must be greater than minimum Reg. Parameter','Invalid Input','modal')
    set(handles.edit_minReg,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
if regStep>maxReg-minReg
    errordlg('Value for step Reg. Parameter out of interval','Invalid Input','modal')
    set(handles.edit_minReg,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');   

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');
function edit_minReg_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function edit_maxReg_Callback(hObject, eventdata, handles)
maxReg = str2double(get(handles.edit_maxReg,'String'));
minReg = str2double(get(handles.edit_minReg,'String'));
regStep = str2double(get(handles.edit_regStep,'String'));
if isnan(maxReg) || maxReg<0
    errordlg('You must enter a positive value (or 0) for maximum Reg. Parameter','Invalid Input','modal')
    set(handles.edit_maxReg,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
if maxReg<minReg
    errordlg('Value for maximum Reg. Parameter must be greater than minimum Reg. Parameter','Invalid Input','modal')
    set(handles.edit_maxReg,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
if regStep>maxReg-minReg
    errordlg('Value for step Reg. Parameter out of interval','Invalid Input','modal')
    set(handles.edit_maxReg,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');   

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');
function edit_maxReg_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function edit_regStep_Callback(hObject, eventdata, handles)
minReg = str2double(get(handles.edit_minReg,'String'));
maxReg = str2double(get(handles.edit_maxReg,'String'));
regStep = str2double(get(handles.edit_regStep,'String'));
if isnan(regStep) || regStep<0
    errordlg('You must enter a positive value (or 0) for step Reg. Parameter','Invalid Input','modal')
    set(handles.edit_regStep,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
if regStep>maxReg-minReg
    errordlg('Value for step Reg. Parameter out of interval','Invalid Input','modal')
    set(handles.edit_regStep,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');    

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');
function edit_regStep_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function text_rangeKer_CreateFcn(hObject, eventdata, handles)

set(hObject,'Visible','off');

function edit_minKer_Callback(hObject, eventdata, handles)
minKer = str2double(get(handles.edit_minKer, 'String'));
maxKer = str2double(get(handles.edit_maxKer, 'String'));
stepKer = str2double(get(handles.edit_kerStep, 'String'));

if isnan(minKer) || minKer<0
    errordlg('You must enter a positive value (or 0) for minimum Ker. Parameter','Invalid Input','modal')
    set(handles.edit_minKer,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
if maxKer<minKer
    errordlg('Value for maximum Ker. Parameter must be greater than minimum Ker. Parameter','Invalid Input','modal')
    set(handles.edit_minKer,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
if stepKer>maxKer-minKer
    errordlg('Value for step Ker. Parameter out of range','Invalid Input','modal')
    set(handles.edit_minKer,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');   

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');
function edit_minKer_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'Visible','off');
function edit_maxKer_Callback(hObject, eventdata, handles)
minKer = str2double(get(handles.edit_minKer, 'String'));
maxKer = str2double(get(handles.edit_maxKer, 'String'));
stepKer = str2double(get(handles.edit_kerStep, 'String'));

if isnan(maxKer) || maxKer<0
    errordlg('You must enter a positive value (or 0) for maximum Ker. Parameter','Invalid Input','modal')
    set(handles.edit_maxKer,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
if maxKer<minKer
    errordlg('Value for maximum Ker. Parameter must be greater than minimum Ker. Parameter','Invalid Input','modal')
    set(handles.edit_maxKer,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
if stepKer>maxKer-minKer
    errordlg('Value for step Ker. Parameter out of range','Invalid Input','modal')
    set(handles.edit_maxKer,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');    

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');
function edit_maxKer_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'Visible','off');
function edit_kerStep_Callback(hObject, eventdata, handles)
minKer = str2double(get(handles.edit_minKer, 'String'));
maxKer = str2double(get(handles.edit_maxKer, 'String'));
stepKer = str2double(get(handles.edit_kerStep, 'String'));
if isnan(stepKer) || stepKer<0
    errordlg('You must enter a positive value (or 0) for step Ker. Parameter','Invalid Input','modal')
    set(handles.edit_kerStep,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
if stepKer>maxKer-minKer
    errordlg('Value for step Ker. Parameter out of range','Invalid Input','modal')
    set(handles.edit_kerStep,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');   

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');
function edit_kerStep_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'Visible','off');
function text_bestKerPar_CreateFcn(hObject, eventdata, handles)

set(hObject,'Visible','off')
function checkbox_geomKer_CreateFcn(hObject, eventdata, handles)

% Deactivate kernel parameter geometrical sequence checkbox
set(hObject,'Visible','off')

function checkbox_geomReg_Callback(hObject, eventdata, handles)

set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');           

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');

function checkbox_geomKer_Callback(hObject, eventdata, handles)
set(handles.pushbutton_test,'Enable','off');
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

set(handles.text_bestRegPar, 'String', '---');
set(handles.text_bestKerPar, 'String', '---');           

set(handles.pushbutton_plotParams,'Visible','off');
set(handles.pushbutton_saveDataParams,'Visible','off');
set(handles.pushbutton_saveBothParams,'Visible','off');
set(handles.pushbutton_saveFigParams,'Visible','off');
set(handles.checkbox_fixKerParam,'Visible','off');
set(handles.checkbox_fixRegParam,'Visible','off');
set(handles.checkbox_trainErr,'Visible','off');
set(handles.checkbox_valErr,'Visible','off');

function checkbox_geomReg_CreateFcn(hObject, eventdata, handles)

function pushbutton_train_Callback(hObject, eventdata, handles)

boolTr = isfield(handles, 'Xtr') && isfield(handles,'Ytr');
boolHO = strcmp(get(get(handles.uipanel_paramsel_type , 'SelectedObject') , 'String'), 'Hold out');

% Get selected algorithm
algolist = get(handles.popupmenu_algorithm, 'String');
algo = get(handles.popupmenu_algorithm, 'Value');
handles.algorithm = algolist{algo}; % Store in handles struct

% Get selected kernel
kerlist = get(handles.popupmenu_kernel, 'String');
ker = get(handles.popupmenu_kernel, 'Value');

if boolTr

    switch algolist{algo}
        
        case 'k Nearest Neighbors'
            
            % HP selection
            if boolHO
                
                minReg = str2double(get(handles.edit_minReg , 'String'));
                regStep = str2double(get(handles.edit_regStep , 'String'));
                maxReg = str2double(get(handles.edit_maxReg , 'String'));
                if isnan(minReg) || minReg<0
                    errordlg('You must enter a positive value (or 0) for minimum Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_minReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if minReg-floor(minReg)>0
                    errordlg('In k-NN minimum Reg. Parameter must be integer','Invalid Input','modal')
                    set(handles.edit_minReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(regStep) || regStep<0
                    errordlg('You must enter a positive value (or 0) for step Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_regStep,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if regStep-floor(regStep)>0
                    errordlg('In k-NN step Reg. Parameter must be integer','Invalid Input','modal')
                    set(handles.edit_regStep,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(maxReg) || maxReg<0
                    errordlg('You must enter a positive value (or 0) for maximum Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_maxReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if maxReg-floor(maxReg)>0
                    errordlg('In k-NN maximum Reg. Parameter must be integer','Invalid Input','modal')
                    set(handles.edit_maxReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if maxReg>size(handles.Xtr,1)
                    errordlg('In k-NN maximum Reg. Parameter must be inferior (or equal) to the number of Tr points','Invalid Input','modal')
                    set(handles.edit_maxReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                    
                % Generate candidate intK vector
                logX = (get(handles.checkbox_geomReg , 'Value') == 1);
                intK = makeRegParRange(minReg, regStep , maxReg , logX);
                
                [k, ~, Vm, ~, Tm, ~] = holdoutCV('knn', handles.Xtr, handles.Ytr, [], str2double(get(handles.edit_valPerc, 'String'))/100, str2double(get(handles.edit_HORepetitions, 'String')), intK, [])

                % Print best K
                set(handles.text_bestRegPar, 'String', num2str(k));
                
                % Set handles fields (for later usage)
                handles.lambda = k;
                handles.Vm = Vm';
                handles.Tm = Tm';
                handles.logX = logX;
                handles.logY = 0;
                handles.intRegPar = intK;
                handles.intKerPar = [];
                
            else
                regPar = str2double(get(handles.edit_regPar , 'String'));
                if isnan(regPar) || regPar<0
                    errordlg('You must enter a positive value (or 0) for Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_regPar,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if regPar-floor(regPar)>0
                    errordlg('In k-NN the Reg. Parameter must be integer','Invalid Input','modal')
                    set(handles.edit_regPar,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if regPar>size(handles.Xtr,1)
                    errordlg('In k-NN the Reg. Parameter must be inferior (or equal) to the number of Tr points','Invalid Input','modal')
                    set(handles.edit_regPar,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                handles.lambda = regPar;
            end
            
            % No training
            
            condition2 = 1;
            
            msgbox('Done!');
            
        case 'Regularized Least Squares'

            % HP selection
            if boolHO

                minReg = str2double(get(handles.edit_minReg , 'String'));
                regStep = str2double(get(handles.edit_regStep , 'String'));
                maxReg = str2double(get(handles.edit_maxReg , 'String'));
                if isnan(minReg) || minReg<0
                    errordlg('You must enter a positive value (or 0) for minimum Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_minReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(regStep) || regStep<0
                    errordlg('You must enter a positive value (or 0) for step Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_regStep,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(maxReg) || maxReg<0
                    errordlg('You must enter a positive value (or 0) for maximum Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_maxReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end

                % Generate candidate intLambda vector
                logX = (get(handles.checkbox_geomReg , 'Value') == 1);
                intLambda = makeRegParRange(minReg, regStep , maxReg , logX);
 
                % Execute HO parameter selection
                [lambda, Vm, ~, Tm, ~] = holdoutCVRLS( handles.Xtr, handles.Ytr, str2double(get(handles.edit_valPerc, 'String'))/100, str2double(get(handles.edit_HORepetitions, 'String')), intLambda);

                % Print best lambda
                set(handles.text_bestRegPar, 'String', num2str(lambda));
                    
                % Set handles fields (for later usage)
                handles.lambda = lambda;
                handles.Vm = Vm;
                handles.Tm = Tm;
                handles.logX = logX;
                handles.logY = 0;
                handles.intRegPar = intLambda;
                handles.intKerPar = [];
        
            else                       
                regPar = str2double(get(handles.edit_regPar , 'String'));
                if isnan(regPar) || regPar<0
                    errordlg('You must enter a positive value (or 0) for Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_regPar,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                handles.lambda = regPar;
            end
            
            % Training
            handles.w = regularizedLSTrain(handles.Xtr, handles.Ytr, handles.lambda);
            
            condition2 = 1;
            
            msgbox('Done!');
            
        case 'Logistic Regression'
            
            % HP selection
            if boolHO
                
                minReg = str2double(get(handles.edit_minReg , 'String'));
                regStep = str2double(get(handles.edit_regStep , 'String'));
                maxReg = str2double(get(handles.edit_maxReg , 'String'));
                if isnan(minReg) || minReg<0
                    errordlg('You must enter a positive value (or 0) for minimum Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_minReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(regStep) || regStep<0
                    errordlg('You must enter a positive value (or 0) for step Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_regStep,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(maxReg) || maxReg<0
                    errordlg('You must enter a positive value (or 0) for maximum Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_maxReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                
                % Generate candidate intLambda vector
                logX = (get(handles.checkbox_geomReg , 'Value') == 1);
                intLambda = makeRegParRange(minReg, regStep , maxReg , logX);
                
                % Execute HO parameter selection
                [lambda, ~, Vm, ~, Tm, ~] = holdoutCV('lr', handles.Xtr, handles.Ytr,[], str2double(get(handles.edit_valPerc, 'String'))/100, str2double(get(handles.edit_HORepetitions, 'String')) , intLambda, []);
                                
                % Print best lambda
                set(handles.text_bestRegPar, 'String', num2str(lambda));
                
                % Set handles fields (for later usage)
                handles.lambda = lambda;
                handles.Vm = Vm';
                handles.Tm = Tm';
                handles.logX = logX;
                handles.logY = 0;
                handles.intRegPar = intLambda;
                handles.intKerPar = [];
                
            else              
                regPar = str2double(get(handles.edit_regPar , 'String'));
                if isnan(regPar) || regPar<0
                    errordlg('You must enter a positive value (or 0) for Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_regPar,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                handles.lambda = regPar;
            end
            
            % Training
            handles.w = linearLRTrain(handles.Xtr, handles.Ytr, handles.lambda);
            
            condition2 = 1;
            
            msgbox('Done!');
            
        case 'Kernel Regularized Least Squares'
            
            switch kerlist{ker}
                case 'Linear Kernel'
                    handles.kernel  = 'linear';

                case 'Gaussian Kernel'
                    handles.kernel  = 'gaussian';

                case 'Polynomial Kernel'
                    handles.kernel  = 'polynomial';
            end
			
			
            % HP selection
            if boolHO

                minReg = str2double(get(handles.edit_minReg , 'String'));
                regStep = str2double(get(handles.edit_regStep , 'String'));
                maxReg = str2double(get(handles.edit_maxReg , 'String'));
                if isnan(minReg) || minReg<0
                    errordlg('You must enter a positive value (or 0) for minimum Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_minReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(regStep) || regStep<0
                    errordlg('You must enter a positive value (or 0) for step Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_regStep,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(maxReg) || maxReg<0
                    errordlg('You must enter a positive value (or 0) for maximum Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_maxReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                minKer = str2double(get(handles.edit_minKer , 'String'));
                kerStep = str2double(get(handles.edit_kerStep , 'String'));
                maxKer = str2double(get(handles.edit_maxKer , 'String'));
                
                if (isnan(minKer) || minKer<0) && strcmp(handles.kernel , 'linear') == 0

                    errordlg('You must enter a positive value (or 0) for minimum Ker. Parameter','Invalid Input','modal')
                    set(handles.edit_minKer,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(kerStep) || kerStep<0
                    errordlg('You must enter a positive value (or 0) for step Ker. Parameter','Invalid Input','modal')
                    set(handles.edit_kerStep,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(maxKer) || maxKer<0
                    errordlg('You must enter a positive value (or 0) for maximum Ker. Parameter','Invalid Input','modal')
                    set(handles.edit_maxKer,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end

                % Generate candidate intLambda vector
                logX = (get(handles.checkbox_geomReg , 'Value') == 1);
                logY = (get(handles.checkbox_geomKer , 'Value') == 1);
                intLambda = makeRegParRange(minReg, regStep , maxReg , logX);
                intKerPar = makeRegParRange(minKer, kerStep , maxKer , logY);
                              
                % Execute HO parameter selection
                %[lambda, sigma, Vm, ~, Tm, ~] = holdoutCV('krls', handles.Xtr, handles.Ytr, handles.kernel, str2double(get(handles.edit_valPerc, 'String'))/100, str2double(get(handles.edit_HORepetitions, 'String')), intLambda, intKerPar);
                [lambda, sigma, Vm, ~, Tm, ~] = holdoutCVKernRLS(handles.Xtr, handles.Ytr, handles.kernel, str2double(get(handles.edit_valPerc, 'String'))/100, str2double(get(handles.edit_HORepetitions, 'String')), intLambda, intKerPar);
                
                % Print best lambda & sigma
                set(handles.text_bestRegPar, 'String', num2str(lambda));
                set(handles.text_bestKerPar, 'String', num2str(sigma));
                
                % Set handles fields (for later usage)
                handles.lambda = lambda;
                handles.sigma = sigma;
                handles.Vm = Vm';
                handles.Tm = Tm';
                handles.logX = logX;
                handles.logY = logY;
                handles.intRegPar = intLambda;
                handles.intKerPar = intKerPar;
       
            else               
                regPar = str2double(get(handles.edit_regPar , 'String'));
                if isnan(regPar) || regPar<0
                    errordlg('You must enter a positive value (or 0) for Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_regPar,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                handles.lambda = regPar;
                kerPar = str2double(get(handles.edit_kerPar , 'String'));

                if (isnan(kerPar) || kerPar<0 ) && strcmp(kerlist{ker},'Linear Kernel') == 0                
                    errordlg('You must enter a positive value (or 0) for Ker. Parameter','Invalid Input','modal')
                    set(handles.edit_kerPar,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                handles.sigma = kerPar;
            end

            % Training
            handles.c = regularizedKernLSTrain(handles.Xtr, handles.Ytr, handles.kernel, handles.sigma, handles.lambda);
            
            condition2 = 1;
            
            msgbox('Done!');
            
        case 'Kernel Logistic Regression'
                
            switch kerlist{ker}
                case 'Linear Kernel'
                    handles.kernel  = 'linear';

                case 'Gaussian Kernel'
                    handles.kernel  = 'gaussian';

                case 'Polynomial Kernel'
                    handles.kernel  = 'polynomial';
            end
                
            % HP selection
            if boolHO
                
                minReg = str2double(get(handles.edit_minReg , 'String'));
                regStep = str2double(get(handles.edit_regStep , 'String'));
                maxReg = str2double(get(handles.edit_maxReg , 'String'));
                if isnan(minReg) || minReg<0
                    errordlg('You must enter a positive value (or 0) for minimum Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_minReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(regStep) || regStep<0
                    errordlg('You must enter a positive value (or 0) for step Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_regStep,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(maxReg) || maxReg<0
                    errordlg('You must enter a positive value (or 0) for maximum Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_maxReg,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                minKer = str2double(get(handles.edit_minKer , 'String'));
                kerStep = str2double(get(handles.edit_kerStep , 'String'));
                maxKer = str2double(get(handles.edit_maxKer , 'String'));
                if (isnan(minKer) || minKer<0) && strcmp(handles.kernel , 'linear') == 0
                    errordlg('You must enter a positive value (or 0) for minimum Ker. Parameter','Invalid Input','modal')
                    set(handles.edit_minKer,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(kerStep) || kerStep<0
                    errordlg('You must enter a positive value (or 0) for step Ker. Parameter','Invalid Input','modal')
                    set(handles.edit_kerStep,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                if isnan(maxKer) || maxKer<0
                    errordlg('You must enter a positive value (or 0) for maximum Ker. Parameter','Invalid Input','modal')
                    set(handles.edit_maxKer,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                
                % Generate candidate intLambda vector
                logX = (get(handles.checkbox_geomReg , 'Value') == 1);
                logY = (get(handles.checkbox_geomKer , 'Value') == 1);                
                intLambda = makeRegParRange(minReg, regStep , maxReg , logX);
                intKerPar = makeRegParRange(minKer, kerStep , maxKer , logY);
                
                % Execute HO parameter selection
                [lambda, sigma, Vm, ~, Tm, ~] = holdoutCV('klr', handles.Xtr, handles.Ytr,handles.kernel, str2double(get(handles.edit_valPerc, 'String'))/100, str2double(get(handles.edit_HORepetitions, 'String')), intLambda, intKerPar);
                
                % Print best lambda & sigma
                set(handles.text_bestRegPar, 'String', lambda);
                set(handles.text_bestKerPar, 'String', sigma); 
                
                % Set handles fields (for later usage)
                handles.lambda = lambda;
                handles.sigma = sigma;
                handles.Vm = Vm';
                handles.Tm = Tm';
                handles.logX = logX;
                handles.logY = logY;
                handles.intRegPar = intLambda;
                handles.intKerPar = intKerPar;
             
            else               
                regPar = str2double(get(handles.edit_regPar , 'String'));
                if isnan(regPar) || regPar<0
                    errordlg('You must enter a positive value (or 0) for Reg. Parameter','Invalid Input','modal')
                    set(handles.edit_regPar,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                handles.lambda = regPar;
                kerPar = str2double(get(handles.edit_kerPar , 'String'));
                
                % Get selected kernel
                kerlist = get(handles.popupmenu_kernel, 'String');
                ker = get(handles.popupmenu_kernel, 'Value');

                if (isnan(kerPar) || kerPar<0 ) && strcmp(kerlist{ker},'Linear Kernel') == 0
                    errordlg('You must enter a positive value (or 0) for Ker. Parameter','Invalid Input','modal')
                    set(handles.edit_kerPar,'String','');
                    guidata(hObject, handles);
                    uicontrol(hObject)
                    return
                end
                handles.sigma = kerPar;
            end
            
            % Training
            handles.c = kernLRTrain(handles.Xtr, handles.Ytr, handles.kernel, handles.sigma, handles.lambda);

            condition2 = 1;
            
            msgbox('Done!');
            
        otherwise
            condition2 = 0;
    end
else
    errordlg('Training Set missing','Invalid Input','modal') 
    guidata(hObject, handles);
    uicontrol(hObject)
    return 
end

if boolTr && condition2
    set(handles.pushbutton_test,'Enable','on')
end
set(handles.text_empErr,'String','');
set(handles.text_predConfidence,'String','');

% set(handles.text_bestRegPar, 'String', '---');
% set(handles.text_bestKerPar, 'String', '---');   

set(handles.pushbutton_plotResults,'Visible','off');
set(handles.pushbutton_saveDataResults,'Visible','off');
set(handles.pushbutton_saveBothResults,'Visible','off');
set(handles.pushbutton_saveFigResults,'Visible','off');

set(handles.pushbutton_plotParams,'Visible','on');
set(handles.pushbutton_saveDataParams,'Visible','on');
set(handles.pushbutton_saveBothParams,'Visible','on');
set(handles.pushbutton_saveFigParams,'Visible','on');
% set(handles.checkbox_fixKerParam,'Visible','on');
% set(handles.checkbox_fixRegParam,'Visible','on');
set(handles.checkbox_trainErr,'Visible','on');
set(handles.checkbox_valErr,'Visible','on');

set(handles.pushbutton_saveDataParams,'Enable','on');

if boolHO
    set(handles.pushbutton_plotParams,'Enable','on');
%     set(handles.checkbox_fixKerParam,'Enable','on');
%     set(handles.checkbox_fixRegParam,'Enable','on');
    set(handles.checkbox_trainErr,'Enable','on');
    set(handles.checkbox_valErr,'Enable','on');
else
    set(handles.pushbutton_plotParams,'Enable','off');
end

guidata(hObject, handles);
function pushbutton_train_CreateFcn(hObject, eventdata, handles)

set(hObject,'Enable','on');

function checkbox_trainErr_Callback(hObject, eventdata, handles)
% Hint: get(hObject,'Value') returns toggle state of checkbox_trainErr

if get(hObject,'Value') == 0 && get(handles.checkbox_valErr,'Value') == 0
    set(handles.checkbox_valErr,'Value', 1);
end
function checkbox_valErr_Callback(hObject, eventdata, handles)
% Hint: get(hObject,'Value') returns toggle state of checkbox_valErr

if get(hObject,'Value') == 0 && get(handles.checkbox_trainErr,'Value') == 0
    set(handles.checkbox_trainErr,'Value', 1);
end
function checkbox_fixKerParam_Callback(hObject, eventdata, handles)
% Hint: get(hObject,'Value') returns toggle state of checkbox_fixKerParam

if get(hObject,'Value') == 1 && get(handles.checkbox_fixRegParam,'Value') == 1
    set(handles.checkbox_fixRegParam,'Value', 0);
end
function checkbox_fixKerParam_CreateFcn(hObject, eventdata, handles)

set(hObject,'Visible','off');
function checkbox_fixRegParam_CreateFcn(hObject, eventdata, handles)

set(hObject,'Visible','off');
function checkbox_fixRegParam_Callback(hObject, eventdata, handles)
% Hint: get(hObject,'Value') returns toggle state of checkbox_fixRegParam

if get(hObject,'Value') == 1 && get(handles.checkbox_fixKerParam,'Value') == 1
    set(handles.checkbox_fixKerParam,'Value', 0);
end

function pushbutton_test_Callback(hObject, eventdata, handles)

% Precondition check
if isfield(handles,'algorithm')
    switch handles.algorithm
        case 'k Nearest Neighbors'
            condition = any(strcmp('Xtr',fieldnames(handles)))&&...
                        any(strcmp('Ytr',fieldnames(handles)))&&...
                        any(strcmp('Xte',fieldnames(handles)))&&...
                        any(strcmp('Yte',fieldnames(handles)))&&...
                        any(strcmp('k',fieldnames(handles)));
        case 'Regularized Least Squares'
            condition = any(strcmp('Xte',fieldnames(handles)))&&...
                        any(strcmp('Yte',fieldnames(handles)))&&...
                        any(strcmp('w',fieldnames(handles)));
        case 'Kernel Regularized Least Squares'
            condition = any(strcmp('Xte',fieldnames(handles)))&&...
                        any(strcmp('Yte',fieldnames(handles)))&&...
                        any(strcmp('kernel',fieldnames(handles)))&&...
                        any(strcmp('c',fieldnames(handles)))&&...
                        any(strcmp('sigma',fieldnames(handles)))&&...
                        any(strcmp('lambda',fieldnames(handles)));
        case 'Logistic Regression'
            condition = any(strcmp('Xte',fieldnames(handles)))&&...
                        any(strcmp('Yte',fieldnames(handles)))&&...
                        any(strcmp('w',fieldnames(handles)));
        case 'Kernel Logistic Regression'
            condition = any(strcmp('Xte',fieldnames(handles)))&&...
                        any(strcmp('Xtr',fieldnames(handles)))&&...
                        any(strcmp('Yte',fieldnames(handles)))&&...
                        any(strcmp('kernel',fieldnames(handles)))&&...
                        any(strcmp('c',fieldnames(handles)))&&...
                        any(strcmp('sigma',fieldnames(handles)));
        otherwise
            condition = 0;
    end
else
    condition = 0;
end

if condition

   switch handles.algorithm
        case 'k Nearest Neighbors'
            
            % Predict test labels            
            handles.YtePred = kNNClassify(handles.Xtr, handles.Ytr, handles.lambda, handles.Xte);
            % Compute test error and display it in the text box  
            set(handles.text_empErr, 'String', num2str(1 - sum(handles.YtePred ~= handles.Yte)/size(handles.YtePred,1))); 
            
        case 'Regularized Least Squares'
            
            % Predict test labels
            handles.YtePred = regularizedLSTest(handles.w, handles.Xte);
            % Compute test error and display it in the text box  
            set(handles.text_empErr, 'String' , num2str(1 - sum(handles.YtePred ~= handles.Yte)/size(handles.YtePred,1))); 

        case 'Kernel Regularized Least Squares'
            
            % Predict test labels
            handles.YtePred = regularizedKernLSTest(handles.c, handles.Xtr, handles.kernel, handles.sigma, handles.Xte);
            % Compute test error and display it in the text box  
            set(handles.text_empErr, 'String' , num2str(1 - sum(handles.YtePred ~= handles.Yte)/size(handles.YtePred,1))); 

        case 'Logistic Regression'
            
            % Predict test labels
            [handles.YtePred , handles.ppred] = linearLRTest(handles.w, handles.Xte);
            % Compute test error and display it in the text box  
            set(handles.text_empErr, 'String' , num2str(1 - sum(handles.YtePred ~= handles.Yte)/size(handles.YtePred,1))); 
            set(handles.text_predConfidence, 'String' , num2str(mean(handles.ppred,1))); 
      
        case 'Kernel Logistic Regression'
            
            % Predict test labels
            [handles.YtePred, handles.ppred] = kernLRTest(handles.c, handles.Xtr, handles.kernel, handles.sigma, handles.Xte);
            % Compute test error and display it in the text box  
            set(handles.text_empErr, 'String' , num2str(1 - sum(handles.YtePred ~= handles.Yte)/size(handles.YtePred,1))); 
            set(handles.text_predConfidence, 'String' , num2str(mean(handles.ppred,1))); 

   end
end

set(handles.pushbutton_plotResults,'Visible','on');
set(handles.pushbutton_saveDataResults,'Visible','on');
set(handles.pushbutton_saveBothResults,'Visible','on');
set(handles.pushbutton_saveFigResults,'Visible','on');
set(handles.pushbutton_saveDataResults,'Enable','on');

D = size(handles.Xte,2);
if D<4
    set(handles.pushbutton_plotResults,'Enable','on');
else
    set(handles.pushbutton_plotResults,'Enable','off');
end

guidata(hObject, handles);
function pushbutton_test_CreateFcn(hObject, eventdata, handles)

set(hObject,'Enable','off')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function uipanel_sourceDataset_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel_sourceDataset 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

switch get(eventdata.NewValue,'Tag')
    case 'radiobutton_generatedDataset'
        
        set(handles.pushbutton_generateDataset, 'Visible', 'Off');
        set(handles.pushbutton_loadDataset, 'Visible', 'Off');
        
        set(handles.pushbutton_saveBothDataset, 'Visible', 'Off');
        set(handles.pushbutton_saveDataDataset, 'Visible', 'Off');
        set(handles.pushbutton_saveFigDataset, 'Visible', 'Off');    
        set(handles.pushbutton_saveBothDataset, 'Position', [68.167 2 15 1.75]);
        set(handles.pushbutton_saveFigDataset, 'Position', [49.167 1.062 15 1.75]);
        set(handles.pushbutton_saveDataDataset, 'Position', [49.167 3.062 15 1.75]);
        
        set(handles.pushbutton_plotDataset, 'Visible', 'Off');
        set(handles.pushbutton_plotDataset, 'Position', [29 2 15 1.75]);
              
        set(handles.edit_nTe, 'Visible', 'Off');
        set(handles.edit_nTe, 'String', '');
        set(handles.edit_D, 'Visible', 'Off');
        set(handles.edit_D, 'String', '');
        set(handles.edit_nTr, 'Visible', 'Off');
        set(handles.edit_nTr, 'String', '');
        set(handles.edit_flipPercent, 'Visible', 'Off');
        set(handles.edit_flipPercent, 'String', '');
        
        set(handles.text_nTe, 'Visible', 'Off');
        set(handles.text_nTr, 'Visible', 'Off');
        set(handles.text_D, 'Visible', 'Off');
        set(handles.text_flipPercent, 'Visible', 'Off');
        
        set(handles.text_kindDataset, 'Visible', 'On');
        set(handles.popupmenu_kindDataset, 'Visible', 'On');
        set(handles.popupmenu_kindDataset, 'Value', 1);
        set(handles.text_NGaussians, 'Visible', 'Off');
        set(handles.edit_NGaussians, 'Visible', 'Off');
        set(handles.uitable_datasetSpecs, 'Visible', 'Off');
       
    case 'radiobutton_loadedDataset'
        
        set(handles.pushbutton_generateDataset, 'Visible', 'Off');
        set(handles.pushbutton_loadDataset, 'Visible', 'On');
        
        set(handles.pushbutton_saveBothDataset, 'Visible', 'Off');
        set(handles.pushbutton_saveDataDataset, 'Visible', 'Off');
        set(handles.pushbutton_saveFigDataset, 'Visible', 'Off');
        set(handles.pushbutton_saveBothDataset, 'Position', [68.167 26.875 15 1.75]);
        set(handles.pushbutton_saveFigDataset, 'Position', [49.167 25.938 15 1.75]);
        set(handles.pushbutton_saveDataDataset, 'Position', [49.167 27.938 15 1.75]);
        
        set(handles.pushbutton_plotDataset, 'Visible', 'Off');
        set(handles.pushbutton_plotDataset, 'Position', [29 26.875 15 1.75]);

        set(handles.text_nTr, 'Visible', 'on');
        set(handles.edit_nTr, 'Visible', 'on');
        set(handles.edit_nTr, 'String', '');
        set(handles.edit_nTr, 'Enable', 'off');
        
        set(handles.edit_nTe, 'Visible', 'on');
        set(handles.text_nTe, 'Visible', 'on');
        set(handles.edit_nTe, 'String', '');
        set(handles.edit_nTe, 'Enable', 'off');
        
        set(handles.edit_D, 'Visible', 'on');
        set(handles.text_D, 'Visible', 'on');
        set(handles.edit_D, 'String', '');
        set(handles.edit_D, 'Enable', 'off');
        
        set(handles.text_flipPercent, 'Visible', 'Off');
        set(handles.edit_flipPercent, 'Visible', 'Off');
        
        set(handles.text_kindDataset, 'Visible', 'Off');
        set(handles.popupmenu_kindDataset, 'Visible', 'Off');
        
        set(handles.text_NGaussians, 'Visible', 'Off');
        set(handles.edit_NGaussians, 'Visible', 'Off');
        set(handles.uitable_datasetSpecs, 'Visible', 'Off');
end

guidata(hObject, handles);

function edit_nTe_Callback(hObject, eventdata, handles)

str = get(handles.popupmenu_kindDataset,'String');
val = get(handles.popupmenu_kindDataset, 'Value');

nTe = str2double(get(handles.edit_nTe,'String'));
if isnan(nTe) || nTe<0 || nTe==0 || nTe-floor(nTe)>0 
    errordlg('You must enter a positive integer value for nTe','Invalid Input','modal')
    set(handles.edit_nTe, 'String', '');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
function edit_nTe_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_nTr_Callback(hObject, eventdata, handles)

str = get(handles.popupmenu_kindDataset,'String');
val = get(handles.popupmenu_kindDataset, 'Value');

nTr = str2double(get(handles.edit_nTr,'String'));
if isnan(nTr) || nTr<0 || nTr==0 || nTr-floor(nTr)>0 
    errordlg('You must enter a positive integer value for nTr','Invalid Input','modal')
    set(handles.edit_nTr, 'String', '');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end

if strcmp(str{val}, '2 Moons') && nTr>100
    errordlg('2 Moons dataset for training is composed by 100 points: nTr in [1,100]','Invalid Input','modal')
    set(handles.edit_nTr, 'String', '');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end       
function edit_nTr_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_flipPercent_Callback(hObject, eventdata, handles)

flipPercent = str2double(get(handles.edit_flipPercent,'String'));
if isnan(flipPercent) || flipPercent<0 || flipPercent>100
    errordlg('You must enter a value in [0,100] for Flip %','Invalid Input','modal')
    set(handles.edit_flipPercent,'String','');
    guidata(hObject, handles);
    uicontrol(hObject)
    return
end
set(handles.pushbutton_plotDataset, 'Visible', 'off');
set(handles.pushbutton_saveDataDataset, 'Visible', 'off');
set(handles.pushbutton_saveBothDataset, 'Visible', 'off');
set(handles.pushbutton_saveFigDataset, 'Visible', 'off');
function edit_flipPercent_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_D_Callback(hObject, eventdata, handles)

D = str2double(get(handles.edit_D,'String'));
if isnan(D) || D<0 || D==0 || D-floor(D)>0
    errordlg('You must enter a positive integer value for D','Invalid Input','modal')
    uicontrol(hObject)
    return
end
set(handles.pushbutton_plotDataset, 'Visible', 'off');
set(handles.pushbutton_saveBothDataset, 'Visible', 'Off');
set(handles.pushbutton_saveDataDataset, 'Visible', 'Off');
set(handles.pushbutton_saveFigDataset, 'Visible', 'Off');
function edit_D_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function popupmenu_kindDataset_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function popupmenu_kindDataset_Callback(hObject, eventdata, handles)

str = get(hObject,'String');
val = get(hObject,'Value');
switch str{val}
    case '...'
        set(handles.uitable_datasetSpecs, 'Visible', 'Off');
        set(handles.text_NGaussians, 'Visible', 'Off');
        set(handles.edit_NGaussians, 'Visible', 'Off');
        
        set(handles.text_nTe, 'Visible', 'Off');
        set(handles.edit_nTe, 'Visible', 'Off');
        set(handles.edit_nTe, 'String', '');
        set(handles.text_D, 'Visible', 'Off');
        set(handles.edit_D, 'Visible', 'Off');
        set(handles.edit_D, 'String', '');
        set(handles.text_nTr, 'Visible', 'Off');
        set(handles.edit_nTr, 'Visible', 'Off');
        set(handles.edit_nTr, 'String', '');
        set(handles.text_flipPercent, 'Visible', 'Off');
        set(handles.edit_flipPercent, 'Visible', 'Off');
        set(handles.edit_flipPercent, 'String', '');
        
        set (handles.pushbutton_generateDataset, 'Visible', 'Off');
        
        set(handles.pushbutton_saveBothDataset, 'Visible', 'Off');
        set(handles.pushbutton_saveDataDataset, 'Visible', 'Off');
        set(handles.pushbutton_saveFigDataset, 'Visible', 'Off');
        set(handles.pushbutton_plotDataset, 'Visible', 'Off');
   
    case '2 Moons'
        set(handles.uitable_datasetSpecs, 'Visible', 'Off');
        set(handles.text_NGaussians, 'Visible', 'Off');
        set(handles.edit_NGaussians, 'Visible', 'Off');
        
        set(handles.text_nTe, 'Visible', 'On');
        set(handles.edit_nTe, 'Visible', 'On');
        set(handles.edit_nTe, 'Enable', 'Off');
        set(handles.edit_nTe, 'String', '200');
        
        set(handles.text_D, 'Visible', 'On');
        set(handles.edit_D, 'Visible', 'On');
        set(handles.edit_D, 'Enable', 'Off');
        set(handles.edit_D, 'String', '2');
        
        set(handles.text_nTr, 'Visible', 'On');
        set(handles.edit_nTr, 'Visible', 'On');
        set(handles.edit_nTr, 'Enable', 'On');
        set(handles.edit_nTr, 'String', '');
        
        set(handles.text_flipPercent, 'Visible', 'On');
        set(handles.edit_flipPercent, 'Visible', 'On');
        set(handles.edit_flipPercent, 'String', '');
        
        set (handles.pushbutton_generateDataset, 'Visible', 'On');
        
        set(handles.pushbutton_saveBothDataset, 'Visible', 'Off');
        set(handles.pushbutton_saveDataDataset, 'Visible', 'Off');
        set(handles.pushbutton_saveFigDataset, 'Visible', 'Off');
        
        set(handles.pushbutton_plotDataset, 'Visible', 'Off');
  
    case 'N Gaussians'
        set(handles.uitable_datasetSpecs, 'Visible', 'Off');
        set(handles.text_NGaussians, 'Visible', 'On');
        set(handles.edit_NGaussians, 'Visible', 'On');  
        set(handles.edit_NGaussians, 'String', '');
        
        set(handles.text_nTe, 'Visible', 'On');
        set(handles.edit_nTe, 'Visible', 'On');
        set(handles.edit_nTe, 'Enable', 'Off');
        set(handles.edit_nTe, 'String', '');
        
        set(handles.text_D, 'Visible', 'On');
        set(handles.edit_D, 'Visible', 'On');
        set(handles.edit_D, 'Enable', 'On');
        set(handles.edit_D, 'String', '');
        
        set(handles.text_nTr, 'Visible', 'On');
        set(handles.edit_nTr, 'Visible', 'On');
        set(handles.edit_nTr, 'Enable', 'Off');
        set(handles.edit_nTr, 'String', '');
        
        set(handles.text_flipPercent, 'Visible', 'On');
        set(handles.edit_flipPercent, 'Visible', 'On');
        set(handles.edit_flipPercent, 'String', '');
        
        set(handles.pushbutton_generateDataset, 'Visible', 'Off');
        
        set(handles.pushbutton_saveBothDataset, 'Visible', 'Off');
        set(handles.pushbutton_saveDataDataset, 'Visible', 'Off');
        set(handles.pushbutton_saveFigDataset, 'Visible', 'Off');
        
        set(handles.pushbutton_plotDataset, 'Visible', 'Off');
end

guidata(hObject, handles);

function edit_NGaussians_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function edit_NGaussians_Callback(hObject, eventdata, handles)

nRowsNew = str2double(get(hObject,'String'));

if isempty(get(hObject,'String')) || nRowsNew==0
    
    set(handles.uitable_datasetSpecs, 'Data', []);
    set(handles.uitable_datasetSpecs, 'Visible', 'Off');
    set(handles.pushbutton_plotDataset, 'Visible', 'off');
    set(handles.pushbutton_generateDataset, 'Visible', 'Off');
    set(handles.edit_nTr,'String','');
    set(handles.edit_nTe,'String','');
    
elseif isnan(nRowsNew) || nRowsNew<0 || nRowsNew-floor(nRowsNew)>0
    
    errordlg('You must enter a positive integer value (or 0) for N','Invalid Input','modal')
    uicontrol(hObject)
    return
    
else

    dOld = get(handles.uitable_datasetSpecs, 'Data');
    colFormat = get(handles.uitable_datasetSpecs, 'ColumnFormat');
    nCols = size(colFormat,2);

    if isempty(dOld)
        d = cell(nRowsNew,nCols);
    else
        nRowsOld = size(dOld,1);
   
        if nRowsOld<nRowsNew
            d = [dOld; cell(nRowsNew-nRowsOld, nCols)];
            for r=nRowsOld+1:nRowsNew
                d{r,nCols-1} = d{1,nCols-1};
                d{r,nCols} = d{1,nCols};
            end
        else
            d = dOld(1:nRowsNew,:);
        end
    end
    nTrPerClass = str2double(d{1,nCols-1});
    if ~isnan(nTrPerClass)
        nTr = nTrPerClass*nRowsNew;
        set(handles.edit_nTr,'String',num2str(nTr));
    else
        set(handles.edit_nTr,'String','');
    end
    nTePerClass = str2double(d{1,nCols});
    if ~isnan(nTePerClass)
        nTe = nTePerClass*nRowsNew;
        set(handles.edit_nTe,'String',num2str(nTe));
    else
        set(handles.edit_nTe,'String','');
    end
    set(handles.uitable_datasetSpecs, 'Data', d);
    set(handles.uitable_datasetSpecs, 'Visible', 'On');
    set(handles.pushbutton_generateDataset, 'Visible', 'On');
end

set(handles.pushbutton_plotDataset, 'Visible', 'off');
set(handles.pushbutton_saveBothDataset, 'Visible', 'Off');
set(handles.pushbutton_saveDataDataset, 'Visible', 'Off');
set(handles.pushbutton_saveFigDataset, 'Visible', 'Off');

guidata(hObject, handles);

function pushbutton_loadDataset_Callback(hObject, eventdata, handles)

startingFolder = pwd;

defaultFileName = fullfile(startingFolder, '*.mat');
[baseFileName, folder] = uigetfile(defaultFileName, 'Select a .mat file.');
if baseFileName == 0
	% User clicked the Cancel button.
	return;
end
fullFileName = fullfile(folder, baseFileName);
loadedDataset = load(fullFileName);
if isfield(loadedDataset, 'Xte')
    handles.Xte = loadedDataset.Xte;
elseif isfield(loadedDataset, 'Xts')
    handles.Xte = loadedDataset.Xts;
else
    errordlg('Cannot load test points: please rename them as Xte or Xts','Invalid Input','modal')
    uicontrol(hObject)
    return
end
if isfield(loadedDataset, 'Yte')
    handles.Yte = loadedDataset.Yte;
elseif isfield(loadedDataset, 'Yts')
    handles.Yte = loadedDataset.Yts;
else
    errordlg('Cannot load test labels: please rename them as Yte or Yts','Invalid Input','modal')
    uicontrol(hObject)
    return
end    
if isfield(loadedDataset, 'Xtr')
    handles.Xtr = loadedDataset.Xtr;
else
    errordlg('Cannot load training points: please rename them as Xtr','Invalid Input','modal')
    uicontrol(hObject)
    return
end
if isfield(loadedDataset, 'Ytr')
    handles.Ytr = loadedDataset.Ytr;
else
    errordlg('Cannot load training labels: please rename them as Ytr','Invalid Input','modal')
    uicontrol(hObject)
    return
end

D = size(handles.Xte,2);
if D~=size(handles.Xtr,2)
    errordlg('Space dimensions D of loaded Xte and Xtr differ','Invalid Input','modal')
    uicontrol(hObject)
    return
end
set(handles.edit_D,'String',num2str(D));
nTr = size(handles.Xtr,1);
if nTr~=size(handles.Ytr,1)
    errordlg('Number of examples of loaded Xtr and Ytr differ','Invalid Input','modal')
    uicontrol(hObject)
    return
end
set(handles.edit_nTr,'String',num2str(nTr));
nTe = size(handles.Xte,1);
if nTe~=size(handles.Yte,1)
    errordlg('Number of examples of loaded Xte and Yte differ','Invalid Input','modal')
    uicontrol(hObject)
    return
end
set(handles.edit_nTe,'String',num2str(nTe));

if (D<4)
    set(handles.pushbutton_plotDataset,'Visible','On');
    set(handles.pushbutton_saveBothDataset, 'Visible', 'On');
    set(handles.pushbutton_saveFigDataset, 'Visible', 'On');
end
set(handles.pushbutton_saveDataDataset, 'Visible', 'On');

guidata(hObject, handles);

function pushbutton_generateDataset_Callback(hObject, eventdata, handles)

str = get(handles.popupmenu_kindDataset,'String');
val = get(handles.popupmenu_kindDataset, 'Value');
switch str{val}
    case '2 Moons'
        
        nTr = str2double(get(handles.edit_nTr,'String'));
        if isnan(nTr) || nTr<0 || nTr==0 || nTr-floor(nTr)>0 || nTr>100
            errordlg('You must enter a positive integer value for nTr','Invalid Input','modal')
            uicontrol(hObject)
            return
        end
        flipPercent = str2double(get(handles.edit_flipPercent,'String'));
        if isnan(flipPercent) || flipPercent<0 || flipPercent>100
            errordlg('You must enter a value in [0,100] for Flip percent','Invalid Input','modal')
            uicontrol(hObject)
            return
        end
        
        [handles.Xtr, handles.Ytr, handles.Xte, handles.Yte] = twoMoons(nTr, flipPercent/100);
        
        D = size(handles.Xtr,2);
        if (D ~= 2)
            errordlg('Dimension of loaded ''two_moons.mat'' is not 2','Invalid Input','modal')
            uicontrol(hObject)
            return
        end
        nTe = size(handles.Xte,1);
        if (nTe ~= 200)
            errordlg('Test set point number of loaded ''two_moons.mat'' is not 200','Invalid Input','modal')
            uicontrol(hObject)
            return
        end
 
    case 'N Gaussians'
        
        D = str2double(get(handles.edit_D,'String'));
        if isnan(D) || D<0 || D==0 || D-floor(D)>0
            errordlg('You must enter a positive integer value for D','Invalid Input','modal')
            uicontrol(hObject)
            return
        end
        
        flipPercent = str2double(get(handles.edit_flipPercent,'String'));
        if isnan(flipPercent) || flipPercent<0 || flipPercent>100
            errordlg('You must enter a value in [0,100] for Flip percent','Invalid Input','modal')
            uicontrol(hObject)
            return
        end

       data = get(handles.uitable_datasetSpecs, 'Data');
       nRows = size(data,1);

       if isempty(data{1,4})
           errordlg('Empty field','Invalid Input','modal')
           uicontrol(hObject)
           return
       end
       nTrPerClass = str2double(data{1,4});
       if isnan(nTrPerClass) || nTrPerClass<0 || nTrPerClass==0 || nTrPerClass-floor(nTrPerClass)>0
           errordlg('You must enter a positive integer value for Tr points','Invalid Input','modal')
           uicontrol(hObject)
           return
       end
       
       if isempty(data{1,5})
           errordlg('Empty field','Invalid Input','modal')
           uicontrol(hObject)
           return
       end
       nTePerClass = str2double(data{1,5});
       if isnan(nTePerClass) || nTePerClass<0 || nTePerClass==0 || nTePerClass-floor(nTePerClass)>0
           errordlg('You must enter a positive integer value for Te points','Invalid Input','modal')
           uicontrol(hObject)
           return
       end
       
       handles.Xtr = zeros(nRows*nTrPerClass,D);
       handles.Ytr = zeros(nRows*nTrPerClass,1);
       handles.Xte = zeros(nRows*nTePerClass,D);
       handles.Yte = zeros(nRows*nTePerClass,1);
        
       for r=1:nRows
           
           if isempty(data{r,1})
               errordlg('Empty field','Invalid Input','modal')
               uicontrol(hObject)
               return
           end
           m = str2num(data{r,1});
           if isempty(m)
               errordlg('Cannot convert expression with ''str2num''','Invalid Input','modal')
               uicontrol(hObject)
               return
           end
           if ~isequal(size(m),[D 1]) && ~isequal(size(m),[1 D])
               errordlg('Mean must be Dx1 (or 1xD)','Invalid Input','modal')
               uicontrol(hObject)
               return
           end
           
           if isempty(data{r,2})
               errordlg('Empty field','Invalid Input','modal')
               uicontrol(hObject)
               return
           end
           cov = str2num(data{r,2});
           if isempty(cov)
               errordlg('Cannot convert the expression with ''str2num''','Invalid Input','modal')
               uicontrol(hObject)
               return
           end
           if ~isequal(size(cov), [D D])
               errordlg('Covariance must be DxD','Invalid Input','modal')
               uicontrol(hObject)
               return
           end
           
           if isempty(data{r,3})
               errordlg('Empty field','Invalid Input','modal')
               uicontrol(hObject)
               return
           end
           lbl = str2double(data{r,3});
           if isnan(lbl) || lbl-floor(lbl)>0
               errordlg('You must enter an integer value for label','Invalid Input','modal')
               uicontrol(hObject)
               return
           end

           [handles.Xtr(((r-1)*nTrPerClass+1):r*nTrPerClass,:), handles.Ytr(((r-1)*nTrPerClass+1):r*nTrPerClass)] = AnisotropicMixGauss(m(:), cov, lbl, nTrPerClass);
           [handles.Xte(((r-1)*nTePerClass+1):r*nTePerClass,:), handles.Yte(((r-1)*nTePerClass+1):r*nTePerClass)] = AnisotropicMixGauss(m(:), cov, lbl, nTePerClass);
           handles.Ytr(((r-1)*nTrPerClass+1):r*nTrPerClass) = flipLabels(handles.Ytr(((r-1)*nTrPerClass+1):r*nTrPerClass), flipPercent/100);
           handles.Yte(((r-1)*nTePerClass+1):r*nTePerClass) = flipLabels(handles.Yte(((r-1)*nTePerClass+1):r*nTePerClass), flipPercent/100);
       end
end

if (D<4)
    set(handles.pushbutton_saveBothDataset, 'Visible', 'On');
    %set(handles.pushbutton_saveBothDataset, 'Enable', 'Off');
    set(handles.pushbutton_saveFigDataset, 'Visible', 'On');
    %set(handles.pushbutton_saveFigDataset, 'Enable', 'Off');
    set(handles.pushbutton_plotDataset, 'Visible', 'On');
end

set(handles.pushbutton_saveDataDataset, 'Visible', 'On');

guidata(hObject, handles);

function uitable_datasetSpecs_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to uitable_datasetSpecs (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

col = eventdata.Indices(2);
row = eventdata.Indices(1);
data = get(hObject, 'Data');
nCols = size(data,2);
nRows = size(data,1);

data{row,col} = eventdata.EditData;

if (col==nCols || col==nCols-1)
    for r=1:nRows
        data(r,col) = data(row,col);
    end
end
set(hObject, 'Data', data);

if col==nCols-1
    nTrPerClass = str2double(data{1,col});
    if ~isnan(nTrPerClass)
        nTr = nTrPerClass*nRows;
        set(handles.edit_nTr,'String',num2str(nTr));
    else
        set(handles.edit_nTr,'String','');
    end
end
if col==nCols
    nTePerClass = str2double(data{1,col});
    if ~isnan(nTePerClass)
        nTe = nTePerClass*nRows;
        set(handles.edit_nTe,'String',num2str(nTe));
    else
        set(handles.edit_nTe,'String','');
    end
end

set(handles.pushbutton_plotDataset, 'Visible', 'off');
set(handles.pushbutton_saveBothDataset, 'Visible', 'Off');
set(handles.pushbutton_saveDataDataset, 'Visible', 'Off');
set(handles.pushbutton_saveFigDataset, 'Visible', 'Off');

guidata(hObject, handles);
function uitable_datasetSpecs_CreateFcn(hObject, eventdata, handles)

set(hObject, 'Data', []);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function pushbutton_saveBothDataset_Callback(hObject, eventdata, handles)

if isfield(handles,'Xte') && ~isempty(handles.Xte)
    Xte = handles.Xte;
else
    errordlg('Missing Xte, cannot save','Invalid Input','modal')
    uicontrol(hObject)
    return
end

if isfield(handles,'Xtr') && ~isempty(handles.Xtr)
    Xtr = handles.Xtr;
else
    errordlg('Missing Xtr, cannot save','Invalid Input','modal')
    uicontrol(hObject)
    return
end   

if isfield(handles,'Yte') && ~isempty(handles.Yte)
    Yte = handles.Yte;
else
    errordlg('Missing Yte, cannot save','Invalid Input','modal')
    uicontrol(hObject)
    return
end  

if isfield(handles,'Ytr') && ~isempty(handles.Ytr)
    Ytr = handles.Ytr;
 errordlg('Missing Ytr, cannot save','Invalid Input','modal')
    uicontrol(hObject)
    return
end 

path = uigetdir(pwd,'Select path where to save data (as .mat) and figures (as .fig):');

if path~=0
    prompt = {'Dataset prefix:'};
    dlg_title = 'Set a prefix for data and figure names:';
    num_lines = 1;
    def = {'dataset'};
    options.Resize='on';
    options.WindowStyle='normal';
    prefix = inputdlg(prompt,dlg_title,num_lines,def,options);
    if ~isempty(prefix)
        save(fullfile(path, [str2mat(prefix) '_data.mat']), 'Xte','Yte', 'Xtr', 'Ytr');
        for figIdx=1:numel(handles.figDataset)
            subfix = get(handles.figDataset(figIdx), 'Name');
            saveas(handles.figDataset(figIdx), fullfile(path, [str2mat(prefix) '_' subfix '.fig']));
        end
    end
end
function pushbutton_saveBothParams_Callback(hObject, eventdata, handles)

boolHO = strcmp(get(get(handles.uipanel_paramsel_type , 'SelectedObject') , 'String'), 'Hold out');
boolkNN = strcmp(handles.algorithm,'k Nearest Neighbors');
boolKernel = strcmp(handles.algorithm,'Kernel Regularized Least Squares') || strcmp(handles.algorithm,'Kernel Logistic Regression');

if isfield(handles,'lambda') && ~isempty(handles.lambda)
    lambda = handles.lambda;
else
    errordlg('Missing lambda, cannot save','Invalid Input','modal')
    uicontrol(hObject)
    return
end

if boolHO
    
    if isfield(handles,'Vm') && ~isempty(handles.Vm)
        Vm = handles.Vm;
    else
        errordlg('Missing Vm, cannot save','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    
    if isfield(handles,'Tm') && ~isempty(handles.Tm)
        Tm = handles.Tm;
    else
        errordlg('Missing Tm, cannot save','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    
end

if boolKernel
    
    if isfield(handles,'sigma') && ~isempty(handles.sigma)
        sigma = handles.sigma;
    else
        errordlg('Missing sigma, cannot save','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    
    if isfield(handles,'c') && ~isempty(handles.c)
    c = handles.c;
    else
        errordlg('Missing c, cannot save','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    
elseif ~boolKernel && ~boolkNN
    
    if isfield(handles,'w') && ~isempty(handles.w)
    w = handles.w;
    else
        errordlg('Missing w, cannot save','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    
end

path = uigetdir(pwd,'Select path where to save data (as .mat) and figures (as .fig):');

if path~=0
    prompt = {'Prefix:'};
    dlg_title = 'Set a prefix for data and figure names:';
    num_lines = 1;
    def = {'train_val_err'};
    options.Resize='on';
    options.WindowStyle='normal';
    prefix = inputdlg(prompt,dlg_title,num_lines,def,options);
    if ~isempty(prefix)
        
        if boolkNN
            save(fullfile(path, [str2mat(prefix) '_data.mat']), 'lambda');
        else
            if ~boolHO && ~boolKernel
                save(fullfile(path, [str2mat(prefix) '_data.mat']), 'lambda', 'w');
            elseif boolHO && ~boolKernel
                save(fullfile(path, [str2mat(prefix) '_data.mat']), 'lambda', 'w', 'Vm','Tm');
            elseif ~boolHO && boolKernel
                save(fullfile(path, [str2mat(prefix) '_data.mat']), 'lambda', 'c', 'sigma');
            else
                save(fullfile(path, [str2mat(prefix) '_data.mat']), 'lambda', 'c', 'sigma','Vm','Tm');
            end
        end
        
        for figIdx=1:numel(handles.figParams)
            subfix = get(handles.figParams(figIdx), 'Name');
            saveas(handles.figParams(figIdx), fullfile(path, [str2mat(prefix) '_' subfix '.fig']));
        end
        
    end
end
function pushbutton_saveBothResults_Callback(hObject, eventdata, handles)

if isfield(handles,'YtePred') && ~isempty(handles.YtePred)
    YtePred = handles.YtePred;
else
    errordlg('Missing YtePred, cannot save','Invalid Input','modal')
    uicontrol(hObject)
    return
end

path = uigetdir(pwd,'Select path where to save the figures (as .fig):');

if path~=0
    prompt = {'Prefix:'};
    dlg_title = 'Set a prefix for figure names:';
    num_lines = 1;
    def = {'results'};
    options.Resize='on';
    options.WindowStyle='normal';
    prefix = inputdlg(prompt,dlg_title,num_lines,def,options);

    if ~isempty(prefix)
        save(fullfile(path, [str2mat(prefix) '_data.mat']), 'YtePred');
        for figIdx=1:numel(handles.figResults)
            subfix = get(handles.figResults(figIdx), 'Name');
        saveas(handles.figResults(figIdx), fullfile(path, [cell2mat(prefix) '_' subfix '.fig']));
        end
    end
end

function pushbutton_saveDataDataset_Callback(hObject, eventdata, handles)

if isfield(handles,'Xte') && ~isempty(handles.Xte)
    Xte = handles.Xte;
else
    errordlg('Missing Xte, cannot save','Invalid Input','modal')
    uicontrol(hObject)
    return
end

if isfield(handles,'Xtr') && ~isempty(handles.Xtr)
    Xtr = handles.Xtr;
else
    errordlg('Missing Xtr, cannot save','Invalid Input','modal')
    uicontrol(hObject)
    return
end   

if isfield(handles,'Yte') && ~isempty(handles.Yte)
    Yte = handles.Yte;
else
    errordlg('Missing Yte, cannot save','Invalid Input','modal')
    uicontrol(hObject)
    return
end

if isfield(handles,'Ytr') && ~isempty(handles.Ytr)
    Ytr = handles.Ytr;
else
    errordlg('Missing Ytr, cannot save', 'Invalid Input', 'modal');
    uicontrol(hObject);
    return
end

 

uisave({'Xtr','Ytr','Xte','Yte'}, '*.mat');
function pushbutton_saveDataParams_Callback(hObject, eventdata, handles)

boolHO = strcmp(get(get(handles.uipanel_paramsel_type , 'SelectedObject') , 'String'), 'Hold out');
boolkNN = strcmp(handles.algorithm,'k Nearest Neighbors');
boolKernel = strcmp(handles.algorithm,'Kernel Regularized Least Squares') || strcmp(handles.algorithm,'Kernel Logistic Regression');

if isfield(handles,'lambda') && ~isempty(handles.lambda)
    lambda = handles.lambda;
else
    errordlg('Missing lambda, cannot save','Invalid Input','modal')
    uicontrol(hObject)
    return
end

if boolHO
    
    if isfield(handles,'Vm') && ~isempty(handles.Vm)
        Vm = handles.Vm;
    else
        errordlg('Missing Vm, cannot save','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    
    if isfield(handles,'Tm') && ~isempty(handles.Tm)
        Tm = handles.Tm;
    else
        errordlg('Missing Tm, cannot save','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    
end

if boolKernel
    
    if isfield(handles,'sigma') && ~isempty(handles.sigma)
        sigma = handles.sigma;
    else
        errordlg('Missing sigma, cannot save','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    
    if isfield(handles,'c') && ~isempty(handles.c)
    c = handles.c;
    else
        errordlg('Missing c, cannot save','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    
elseif ~boolKernel && ~boolkNN
    
    if isfield(handles,'w') && ~isempty(handles.w)
    w = handles.w;
    else
        errordlg('Missing w, cannot save','Invalid Input','modal')
        uicontrol(hObject)
        return
    end
    
end

if boolkNN
    uisave({'lambda'}, '*.mat');
else
    if ~boolHO && ~boolKernel
        uisave({'lambda', 'w'}, '*.mat');
    elseif boolHO && ~boolKernel
        uisave({'lambda', 'w', 'Vm','Tm'}, '*.mat');
    elseif ~boolHO && boolKernel
        uisave({'lambda', 'c', 'sigma'}, '*.mat');
    else
        uisave({'lambda', 'c', 'sigma','Vm','Tm'}, '*.mat');
    end
end
function pushbutton_saveDataResults_Callback(hObject, eventdata, handles)

if isfield(handles,'YtePred') && ~isempty(handles.YtePred)
    YtePred = handles.YtePred;
else
    errordlg('Missing YtePred, cannot save','Invalid Input','modal')
    uicontrol(hObject)
    return
end
uisave({'YtePred'}, '*.mat');

function pushbutton_saveFigDataset_Callback(hObject, eventdata, handles)

path = uigetdir(pwd,'Select path where to save the figures (as .fig):');

if path~=0
    prompt = {'Prefix:'};
    dlg_title = 'Set a prefix for figure names:';
    num_lines = 1;
    def = {'dataset'};
    options.Resize='on';
    options.WindowStyle='normal';
    prefix = inputdlg(prompt,dlg_title,num_lines,def,options);

    if ~isempty(prefix)
        for figIdx=1:numel(handles.figDataset)
            subfix = get(handles.figDataset(figIdx), 'Name');
        saveas(handles.figDataset(figIdx), fullfile(path, [cell2mat(prefix) '_' subfix '.fig']));
        end
    end
end
function pushbutton_saveFigParams_Callback(hObject, eventdata, handles)

path = uigetdir(pwd,'Select path where to save the figures (as .fig):');

if path~=0
    prompt = {'Prefix:'};
    dlg_title = 'Set a prefix for figure names:';
    num_lines = 1;
    def = {'train_val_err'};
    options.Resize='on';
    options.WindowStyle='normal';
    prefix = inputdlg(prompt,dlg_title,num_lines,def,options);

    if ~isempty(prefix)
        for figIdx=1:numel(handles.figParams)
            subfix = get(handles.figParams(figIdx), 'Name');
        saveas(handles.figParams(figIdx), fullfile(path, [cell2mat(prefix) '_' subfix '.fig']));
        end
    end
end
function pushbutton_saveFigResults_Callback(hObject, eventdata, handles)

path = uigetdir(pwd,'Select path where to save the figures (as .fig):');

if path~=0
    prompt = {'Prefix:'};
    dlg_title = 'Set a prefix for figure names:';
    num_lines = 1;
    def = {'results'};
    options.Resize='on';
    options.WindowStyle='normal';
    prefix = inputdlg(prompt,dlg_title,num_lines,def,options);

    if ~isempty(prefix)
        for figIdx=1:numel(handles.figResults)
            subfix = get(handles.figResults(figIdx), 'Name');
        saveas(handles.figResults(figIdx), fullfile(path, [cell2mat(prefix) '_' subfix '.fig']));
        end
    end
end

function pushbutton_plotDataset_Callback(hObject, eventdata, handles)

labels = unique(handles.Ytr);
nLabels = numel(labels);
colors = cool(nLabels);
markerRadius = 20; 

nTe = str2double(get(handles.edit_nTe,'String'));
nTr = str2double(get(handles.edit_nTr,'String'));
D = str2double(get(handles.edit_D,'String'));

h = figure();
handles.figDataset = [handles.figDataset h];
set(gcf, 'Name', ['figure_'  num2str(h)]);
set(gcf, 'NumberTitle', 'Off');

colorClass = zeros(nTr,3);
selClass = zeros(nTr,3);
colorVector = zeros(nTr,3);
for l=1:nLabels
    colorClass = repmat(colors(l,:),nTr,1);
    selClass = repmat((handles.Ytr==labels(l)),1,3);
    colorVector = colorVector + colorClass.*selClass;
end

switch D
    case 1
        scatter(handles.Xtr(:,1), zeros(1,nTr), markerRadius, colorVector, 'filled');
    case 2
        scatter(handles.Xtr(:,1), handles.Xtr(:,2), markerRadius, colorVector, 'filled');
    case 3
        scatter3(handles.Xtr(:,1), handles.Xtr(:,2), handles.Xtr(:,3), markerRadius, colorVector, 'filled');
end

hold on;
    
colorClass = zeros(nTe,3);
selClass = zeros(nTe,3);
colorVector = zeros(nTe,3);
for l=1:nLabels
    colorClass = repmat(colors(l,:),nTe,1);
    selClass = repmat((handles.Yte==labels(l)),1,3);
    colorVector = colorVector + colorClass.*selClass; 
end
switch D
    case 1
        scatter(handles.Xte(:,1), zeros(1,nTe), markerRadius, colorVector);
    case 2
        scatter(handles.Xte(:,1), handles.Xte(:,2), markerRadius, colorVector);
    case 3
        scatter3(handles.Xte(:,1), handles.Xte(:,2), handles.Xte(:,3), markerRadius, colorVector);
end

xlabel('x1');
ylabel('x2');
grid on;
leg_handle = legend('TRAIN', 'TEST');
legend_markers = findobj(get(leg_handle, 'Children'), 'Marker', 'o');
set(legend_markers(2), 'MarkerFaceColor', 'k', 'MarkerEdgeColor', 'k');
set(legend_markers(1), 'MarkerFaceColor', 'none', 'MarkerEdgeColor', 'k');

title('DATASET')
annotation = {['D: ' get(handles.edit_D,'String')], ...
    ['nTr: ' get(handles.edit_nTr,'String')], ...
    ['nTe: ' get(handles.edit_nTe,'String')]};

if get(handles.radiobutton_generatedDataset,'Value')
    annotation = [annotation, {['Flip %:' get(handles.edit_flipPercent,'String')]}];
end

x = get(gca,'XLim');
y = get(gca,'YLim');
text(x(1) + (x(2)-x(1))/15, y(2)-(y(2)-y(1))/15, annotation, 'FontWeight', 'bold', 'VerticalAlignment', 'top');

str = get(handles.popupmenu_kindDataset,'String');
val = get(handles.popupmenu_kindDataset,'Value');
if get(handles.radiobutton_generatedDataset,'Value') && strcmp(str{val},'N Gaussians')
    data = get(handles.uitable_datasetSpecs, 'Data');
    for r=1:size(data,1)
        m = str2num(data{r,1});
        if D==1
            m = [m 0];
        end
        text('Interpreter', 'tex', 'Position', m, 'String', [{['\mu=' data{r,1}]}, {['\Sigma='  data{r,2}]}], 'FontWeight', 'bold');
    end
end

set(gca, 'Color', [0.91 0.91 0.91]);
set(gcf,'CloseRequestFcn',{@myCloseFigFcnDataset, handles.figure})

set(handles.pushbutton_saveFigDataset, 'Enable', 'on');
set(handles.pushbutton_saveBothDataset, 'Enable', 'on');

guidata(hObject, handles);
function pushbutton_plotResults_Callback(hObject, eventdata, handles)

labels = unique(handles.Ytr);
nLabels = numel(labels);
colors = cool(nLabels);
markerRadius = 20; 

nTe = str2double(get(handles.edit_nTe,'String'));
nTr = str2double(get(handles.edit_nTr,'String'));
D = str2double(get(handles.edit_D,'String'));

h = figure();
handles.figResults = [handles.figResults h];
set(gcf, 'Name', ['figure_'  num2str(h)]);
set(gcf, 'NumberTitle', 'Off');

colorClass = zeros(nTe,3);
selClass = zeros(nTe,3);
colorVector = zeros(nTe,3);
for l=1:nLabels
    colorClass = repmat(colors(l,:),nTe,1);
    selClass = repmat((handles.Yte==labels(l)),1,3);
    colorVector = colorVector + colorClass.*selClass; 
end

hold on;
            
switch handles.algorithm
    case 'k Nearest Neighbors'
        separatingFkNN(handles.Xtr, handles.Ytr, handles.lambda)
        switch D
            case 1
                scatter(handles.Xte(:,1), zeros(1,nTe), markerRadius, colorVector);
            case 2
                scatter(handles.Xte(:,1), handles.Xte(:,2), markerRadius, colorVector);
            case 3
                scatter3(handles.Xte(:,1), handles.Xte(:,2), handles.Xte(:,3), markerRadius, colorVector);
        end
    case 'Regularized Least Squares'
        separatingFRLS(handles.w, handles.Xte, handles.Yte)
    case 'Kernel Regularized Least Squares'
        separatingFKernRLS(handles.c, handles.Xtr, handles.kernel, handles.sigma, handles.Xte, handles.Yte);
    case 'Logistic Regression'
        separatingLinearLR(handles.w, handles.Xte, handles.Yte)
    case 'Kernel Logistic Regression'
        separatingKernLR(handles.c, handles.Xtr, handles.kernel, handles.sigma, handles.Xte, handles.Yte)
end

hold off;

xlabel('x1');
if D>1
    ylabel('x2');
end
if D>2
    zlabel('x3');
end

grid on;

title('RESULTS')
annotation = {['D: ' get(handles.edit_D,'String')], ...
    ['nTr: ' get(handles.edit_nTr,'String')], ...
    ['nTe: ' get(handles.edit_nTe,'String')], ...
    ['algo: ' handles.algorithm]};

if strcmp(handles.algorithm,'Kernel Regularized Least Squares') || strcmp(handles.algorithm,'Kernel Logistic Regression')
    annotation = [annotation, {['kernel:' handles.kernel]}];
end

annotation = [annotation, {['lambda:' num2str(handles.lambda)]}];

if strcmp(handles.algorithm,'Kernel Regularized Least Squares') || strcmp(handles.algorithm,'Kernel Logistic Regression')
    annotation = [annotation, {['sigma:' num2str(handles.sigma)]}];
end

x = get(gca,'XLim');
y = get(gca,'YLim');
text(x(1) + (x(2)-x(1))/15, y(2)-(y(2)-y(1))/15, annotation, 'FontWeight', 'bold', 'VerticalAlignment', 'top');

set(gca, 'Color', [0.91 0.91 0.91]);
set(gcf,'CloseRequestFcn',{@myCloseFigFcnResults, handles.figure})

set(handles.pushbutton_saveFigResults, 'Enable', 'on');
set(handles.pushbutton_saveBothResults, 'Enable', 'on');

guidata(hObject, handles);

function pushbutton_plotParams_Callback(hObject, ~, handles)

% Get involved object handles 
checkbox_trainErr_value = get(handles.checkbox_trainErr, 'Value');
checkbox_valErr_value = get(handles.checkbox_valErr, 'Value');

h = figure();
handles.figParams = [handles.figParams h];
set(gcf, 'Name', ['figure_'  num2str(h)]);
set(gcf, 'NumberTitle', 'Off');

if size(handles.Vm,1) == 1
    
    % Only 1 hyperparameter (regularization)
    
    colormap([1 0 0; 0 0 1]); % red and blue
    if handles.logX
        if checkbox_trainErr_value == 1 && checkbox_valErr_value == 1
            semilogx(handles.intRegPar, handles.Vm', handles.intRegPar, handles.Tm');
            legend('Mean validation error', 'Mean training error', 'Location', 'Best')
        elseif checkbox_trainErr_value == 0 && checkbox_valErr_value == 1
            semilogx(handles.intRegPar, handles.Vm');
            legend('Mean validation error', 'Location', 'Best')
        elseif checkbox_trainErr_value == 1 && checkbox_valErr_value == 0
            semilogx(handles.intRegPar, handles.Tm');
            legend('Mean training error', 'Location', 'Best')
        end
    else
        if checkbox_trainErr_value == 1 && checkbox_valErr_value == 1
            plot(handles.intRegPar, handles.Vm', handles.intRegPar, handles.Tm');
            legend('Mean validation error', 'Mean training error', 'Location', 'Best')
        elseif checkbox_trainErr_value == 0 && checkbox_valErr_value == 1
            plot(handles.intRegPar, handles.Vm');
            legend('Mean validation error', 'Location', 'Best')
        elseif checkbox_trainErr_value == 1 && checkbox_valErr_value == 0
            plot(handles.intRegPar, handles.Tm');
            legend('Mean training error', 'Location', 'Best')
        end     
    end
        title('Empirical errors w.r.t. regularization hyperparameter')
        xlabel('Regularization hyperparameter')
        ylabel('Empirical error')
        
elseif size(handles.Vm,1) > 1
    
    % Both hyperparameters (regularization and kernel)

    view(gca, [-45 36]);
    hold(gca, 'all');
	
	if handles.logX == 1
		set(gca ,'XScale','log','XMinorTick','on');
	end
	if handles.logY == 1
		set(gca ,'YScale','log','YMinorTick','on');
    end

    if checkbox_trainErr_value == 1 && checkbox_valErr_value == 1
        surf(handles.intRegPar, handles.intKerPar, handles.Vm, 'Parent' , gca, 'FaceColor', [1 0 0]); %first color (red)
        surf(handles.intRegPar, handles.intKerPar, handles.Tm, 'Parent', gca, 'FaceColor', [0 0 1]); %second color (blue)            legend('Medium validation error', 'Medium training error', 'Location', 'Best')
        legend('Mean validation error', 'Mean training error', 'Location', 'Best')
    elseif checkbox_trainErr_value == 0 && checkbox_valErr_value == 1
        surf(handles.intRegPar, handles.intKerPar, handles.Vm, 'Parent' , gca, 'FaceColor', [1 0 0]); %first color (red)
        legend('Mean validation error', 'Location', 'Best')
    elseif checkbox_trainErr_value == 1 && checkbox_valErr_value == 0
        surf(handles.intRegPar, handles.intKerPar, handles.Tm, 'Parent', gca, 'FaceColor', [0 0 1]); %second color (blue)            legend('Medium validation error', 'Medium training error', 'Location', 'Best')
        legend('Mean training error', 'Location', 'Best')
    end
    
    title('Empirical errors w.r.t. regularization hyperparameter')
    xlabel('X : Regularization hyperparameter')
    ylabel('Y : Kernel hyperparameter')
    zlabel('Z : Empirical error')
end

set(gca, 'Color', [0.91 0.91 0.91]);
set(gcf,'CloseRequestFcn',{@myCloseFigFcnParams, handles.figure})

set(handles.pushbutton_saveFigParams, 'Enable', 'on');
set(handles.pushbutton_saveBothParams, 'Enable', 'on');

guidata(hObject, handles);

function myCloseFigFcnDataset(hObject, eventdata, guiHandle)

selection = questdlg('Close Figure?','Close Request Function','Yes','No','Yes'); 
switch selection,
    case 'Yes',
        isFigureHandle = ishandle(guiHandle) && strcmp(get(guiHandle,'type'),'figure');
        if isFigureHandle
            handles = guidata(guiHandle);
            handles.figDataset(handles.figDataset==gcf) = [];
        end
        delete(gcf)
    case 'No'
        return
end

if isFigureHandle
    if (isempty(handles.figDataset))
        set(handles.pushbutton_saveFigDataset, 'Enable', 'off');
        set(handles.pushbutton_saveBothDataset, 'Enable', 'off');
    end
    guidata(guiHandle, handles);
end
function myCloseFigFcnParams(hObject, eventdata, guiHandle)

selection = questdlg('Close Figure?','Close Request Function','Yes','No','Yes'); 
switch selection,
    case 'Yes',
        isFigureHandle = ishandle(guiHandle) && strcmp(get(guiHandle,'type'),'figure');
        if isFigureHandle
            handles = guidata(guiHandle);
            handles.figParams(handles.figParams==gcf) = [];
        end
        delete(gcf)
    case 'No'
        return
end

if isFigureHandle
    if (isempty(handles.figParams))
        set(handles.pushbutton_saveFigParams, 'Enable', 'off');
        set(handles.pushbutton_saveBothParams, 'Enable', 'off');
    end
    guidata(guiHandle, handles);
end
function myCloseFigFcnResults(hObject, eventdata, guiHandle)

selection = questdlg('Close Figure?','Close Request Function','Yes','No','Yes'); 
switch selection,
    case 'Yes',
        isFigureHandle = ishandle(guiHandle) && strcmp(get(guiHandle,'type'),'figure');
        if isFigureHandle
            handles = guidata(guiHandle);
            handles.figResults(handles.figResults==gcf) = [];
        end
        delete(gcf)
    case 'No'
        return
end

if isFigureHandle
    if (isempty(handles.figResults))
        set(handles.pushbutton_saveFigResults, 'Enable', 'off');
        set(handles.pushbutton_saveBothResults, 'Enable', 'off');
    end
    guidata(guiHandle, handles);
end
