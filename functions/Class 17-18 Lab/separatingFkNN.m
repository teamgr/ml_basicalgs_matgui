function separatingFkNN(Xtr,Ytr, k)
% function separatingF(func, par ,x0, x1, step)
% the function classifies points evenly sampled in a visualization area,
% according to the classifier kNNClassify
%
% Xtr - training examples 
% Ytr - training labels
% k - number of neighbors
%
% k = 5;
% [Xtr, Ytr] = MixGauss([[0;0],[1;1]],[0.5,0.25],1000);
% separatingFkNN(Xtr, Ytr, k)
% hold on
% scatter(X(:,1),X(:,2),3*Y);

    step = 0.05;

    x0 = [min(Xtr(:,1)); min(Xtr(:,2))];
    x1 = [max(Xtr(:,1)); max(Xtr(:,2))];

    XGrid = createGridPoints(x0, x1, step);

    YGrid = kNNClassify(Xtr, Ytr, k, XGrid);
    
    intY = [(min(YGrid) + max(YGrid))/2;(min(YGrid) + max(YGrid))/2;];
    
    x = x0(1):step:x1(1);
    y = x0(2):step:x1(2);
    contour(x, y, reshape(YGrid,numel(x),numel(y))', intY);
end


function Grid = createGridPoints(x0, x1, step)
    [X, Y] = meshgrid(x0(2):step:x1(2), x0(1):step:x1(1));
    Grid = [X(:), Y(:)];
end
