function [ypred, ppred] = linearLRTest(w, Xts)
    ypred = Xts*w;
    ppred = exp(ypred)./(1+exp(ypred));
end