function h_fig = plotTrainValErr( handles, Vm, Tm, figID, logX, logY, intRegPar, intKerPar )
%PLOTTRAINVALERR Summary of this function goes here
%   Detailed explanation goes here

if any(strcmp(figID,fieldnames(handles))) == 0
    h_fig = figure('Name', 'Hyperparameter selection errors');
else
    h_fig = figure(handles.(figID));
end

clf
%hold on
if sum(size(Vm)~=1) == 1
    colormap([1 0 0;0 0 1]) %red and blue
    if logX
        %semilogx(intRegPar, Vm', intRegPar, Tm');
        semilogx(intRegPar, Vm', intRegPar, Tm');
    else
        plot(intRegPar, Vm', intRegPar, Tm');
    end
        title('Empirical errors w.r.t. regularization hyperparameter')
        legend('Median validation error','Median training error','Location','Best')
        xlabel('Regularization hyperparameter range') % x-axis label
        ylabel('Empirical error') % y-axis label
        
elseif sum(size(Vm)~=2)
    
    axes1 = axes('Parent',h_fig);
    view(axes1,[45,45]);
    hold(axes1,'all');
	
	if logX == 1
		set(axes1 ,'XScale','log','XMinorTick','on');
	end
	if logY == 1
		set(axes1 ,'YScale','log','YMinorTick','on');
	end
		
    surf(intKerPar, intRegPar, Vm, 'Parent' ,  axes1, 'FaceColor',[1 0 0]); %first color (red)
    surf(intKerPar, intRegPar, Tm, 'Parent',axes1 , 'FaceColor',[0 0 1]); %second color(blue)
    legend('Median validation error','Median training error','Location','Best')
end


end


