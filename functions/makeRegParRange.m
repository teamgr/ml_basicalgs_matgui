function intRegPar = makeRegParRange(minReg, regStep , maxReg , logTag)

if logTag
	intRegPar =  regStep.^[floor(log(minReg)/log(regStep)) : ceil(log(maxReg)/log(regStep))];
else
	intRegPar = minReg: regStep : maxReg;
end

end