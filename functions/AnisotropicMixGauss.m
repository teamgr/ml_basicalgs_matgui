function [X, Y] = AnisotropicMixGauss(mean, cov, label, n)
    d = numel(mean);
    X = randn(n,d)*cov' + ones(n,1)*mean';
    Y = ones(n,1)*label;
end
    
    
