function intKerPar = makeKerParRange(minKer, kerStep , maxKer , handles, logTag)

if logTag
	intKerPar =  kerStep.^[floor(log(minKer)/log(kerStep)) : ceil(log(maxKer)/log(kerStep))];
else
	intKerPar = minKer: kerStep : maxKer;
end
end